<?php

/**
 * VCardParser test - can parse bundled VCF file into CSV
 */

//require_once __DIR__ . '/../vendor/autoload.php';
require_once 'src/VCardParser.php';
require_once 'src/VCardException.php';
require_once 'src/VCard.php';

// load VCardParser classes
use JeroenDesloovere\VCard\VCardParser;

$pathToVCardExample = 'example.vcf';
$parser = VCardParser::parseFromFile($pathToVCardExample);
echo $parser->getCardAtIndex(0)->fullname; // Prints the full name.
echo $parser->getCardAtIndex(1)->fullname; // Prints the full name.

foreach($parser as $vcard) {
    $lastname = $vcard->lastname;
    $firstname = $vcard->firstname;
   // $birthday = $vcard->birthday->format('Y-m-d');
    
    printf("\"%s\",\"%s\",\"%s\"", $lastname, $firstname, $birthday);
    
    echo PHP_EOL;
}
