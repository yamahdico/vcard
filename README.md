vCard PHP Parser
https://sourceforge.net/projects/vcardphp/

VCard PHP library v1.7.1
https://github.com/jeroendesloovere/vcard

VCard PHP library v2.0.0-dev
https://github.com/jeroendesloovere/vcard/tree/2.0.0-dev

vCard.class
https://github.com/facine/vCard/blob/master/vCard.class.php

vcardexp
https://github.com/
